package com.anytrack.api.service;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.anytrack.api.domain.Client;
import com.anytrack.api.domain.Engagement;
import com.anytrack.api.domain.SlalomResource;
import com.anytrack.api.repository.ClientRepository;
import com.anytrack.api.repository.EngagementRepository;
import com.anytrack.api.repository.SlalomResourceRepository;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.validator.routines.UrlValidator;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Random;

/**
 * Created by michael.kong on 2/8/16.
 */
@Service
public class SlalomResourceServiceImpl implements SlalomResourceService {

    @Value("${BENCH}")
    private String bench; //Value fetched from the property file

    @Value("${PROJECT_BENCH}")
    private String project; //Value fetched from the property file

    @Value("${DEFAULT_RESOURCE_AVATAR}")
    private String defaultAvatar;

    @Value("${AWS_ACCESS_KEY_ID}")
    private String AWSAccessKeyID;

    @Value("${AWS_SECRET_ACCESS_KEY}")
    private String AWSSecretAccessKey;

    @Value("${S3_BUCKET_NAME}")
    private String existingBucketName;

    @Value("${AVATAR_RESIZE_RATIO}")
    private int resizeRatio;

    @Value("${BASE_AVATAR_URL}")
    private String baseAvatarURL;

    @Value("${S3_BUCKET_FOLDER_NAME}")
    private String existingBucketFolderName;

    private final int ALLOCATION_MAX = 100;

    public enum MIMEType {jpg, jpeg, png, gif;
        public static boolean contains(String test) {

            for (MIMEType c : MIMEType.values()) {
                if (c.name().equals(test)) {
                    return true;
                }
            }

            return false;
        }

    };

    @Autowired
    private SlalomResourceRepository resourceRepository;

    @Autowired
    private EngagementRepository engagementRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private EngagementService engagementService;


    /*
    ** When creating a new Slalom resource, we will also create an associated bench engagement
     */
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createResource(SlalomResource resource) {
        SlalomResource existing = resourceRepository.findByResourceEmail(resource.getResourceEmail());
        if(existing != null)
            return false;
        else {
            //user did not provide avatar image, set default
            if(resource.getAvatarURL()==null )
                resource.setAvatarURL(defaultAvatar);
            else {
                //User uploaded an image
                String S3URL = uploadImageToS3(resource.getAvatarURL(), resource.getResourceName());
                resource.setAvatarURL(S3URL);
            }
            resourceRepository.save(resource);
            //Create associated bench
            Client clientBench = clientRepository.findByClientName(bench);
            Engagement benchEngagement = new Engagement(project, clientBench, resource, null, null, ALLOCATION_MAX );
            engagementRepository.save(benchEngagement);
            return true;
        }
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public boolean updateResource(SlalomResource resource) {
        SlalomResource previous = resourceRepository.findOne(resource.getResourceId());
        if(previous != null) {
            resource.setCreated(previous.getCreated());
            resource.setCreatedBy(previous.getCreatedBy());
            resource.setActive(previous.isActive());
            if(resource.getAvatarURL() != null)
            {
                String[] schemes = {"http","https"};
                UrlValidator urlValidator = new UrlValidator(schemes);
                //User uploaded a new encoded image string
                if(!urlValidator.isValid(resource.getAvatarURL())) {
                    String S3URL = uploadImageToS3(resource.getAvatarURL(), resource.getResourceName());
                    resource.setAvatarURL(S3URL);
                }
            }
            resourceRepository.save(resource);
            return true;
        }
        else
            return false;
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteResource(SlalomResource resource) {

        Engagement bench = engagementService.findResourceBench(resource);
        resourceRepository.delete(resource);
        engagementRepository.delete(bench);

    }

    @Override
    public Iterable<SlalomResource> findAllResources() {

        return resourceRepository.findAll();
    }

    @Override
    public SlalomResource findOneResource(Long resourceId) {

        return resourceRepository.findOne(resourceId);
    }

    @Override
    public boolean isResourceExist(Long resourceId) {

        return resourceRepository.exists(resourceId);
    }

    public String uploadImageToS3( String base64ImageString, String resourceName ) {
        //Decode the Base64 encoded string into byte array
        // tokenize the data
        String delims="[,]";
        String[] parts = base64ImageString.split(delims);
        String imageString = parts[1];
        byte[] imageByteArray = Base64.decode(imageString );

        InputStream is = new ByteArrayInputStream(imageByteArray);

        //Find out image type
        String mimeType = null;
        String fileExtension = null;
        try {
            mimeType = URLConnection.guessContentTypeFromStream(is);
            String delimiter="[/]";
            String[] tokens = mimeType.split(delimiter);
            fileExtension = tokens[1];
        } catch (IOException ioException){

        }
        if(MIMEType.contains(fileExtension)) {

            //Convert byte array to BufferedImage to resize
            BufferedImage scaledImage = null;
            try {
                BufferedImage bufferedImage = ImageIO.read(is);
                scaledImage = Scalr.resize(bufferedImage, resizeRatio);
            } catch (IOException ioException){

            }

            //Convert resized image back to byte array
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ImageIO.write(scaledImage, fileExtension, baos);
                imageByteArray = baos.toByteArray();
            } catch (IOException ioException) {

            }

            InputStream inputStream = new ByteArrayInputStream(imageByteArray);

            //create S3 client credential
            AWSCredentials credentials = new BasicAWSCredentials( AWSAccessKeyID, AWSSecretAccessKey);
            ClientConfiguration clientConfig = new ClientConfiguration();
            clientConfig.setProtocol(Protocol.HTTP);

            //create a client connection based on credentials
            AmazonS3 s3client = new AmazonS3Client(credentials,clientConfig);

            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentLength(imageByteArray.length);
            meta.setContentType(mimeType);

            //Upload a file
            String SUFFIX = "/";
            String userName = resourceName.replaceAll("\\s+",""); //remove the white space between first name and last name
            //TODO: remove existing image
            String fileName = existingBucketFolderName + SUFFIX + userName + generateUniqueToken(5) + "_profile." + fileExtension;
            s3client.putObject(new PutObjectRequest(existingBucketName, fileName, inputStream, meta));

            return baseAvatarURL+fileName;
        } else  //file contains invalid mime type, set user avatar to default
            return defaultAvatar;

    }


    /**
     * Generate a random hex encoded string token of the specified length
     *
     * @param length
     * @return random hex string
     */
    public static String generateUniqueToken(Integer length)
    {

        byte random[] = new byte[length];
        Random randomGenerator = new Random();
        StringBuffer buffer = new StringBuffer();

        randomGenerator.nextBytes(random);

        for (int j = 0; j < random.length; j++)
        {
            byte b1 = (byte) ((random[j] & 0xf0) >> 4);
            byte b2 = (byte) (random[j] & 0x0f);
            if (b1 < 10)
                buffer.append((char) ('0' + b1));
            else
                buffer.append((char) ('A' + (b1 - 10)));
            if (b2 < 10)
                buffer.append((char) ('0' + b2));
            else
                buffer.append((char) ('A' + (b2 - 10)));
        }

        return (buffer.toString());
    }

}
