package com.anytrack.api.service;

import com.anytrack.api.domain.Client;
import com.anytrack.api.domain.Engagement;
import com.anytrack.api.domain.SlalomResource;
import com.anytrack.api.repository.ClientRepository;
import com.anytrack.api.repository.EngagementRepository;
import com.anytrack.api.repository.SlalomResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by michael.kong on 2/5/16.
 */
@Service
public class EngagementServiceImpl implements EngagementService {

    @Value("${BENCH}")
    private String bench; //Value fetched from the property file

    @Autowired
    private EngagementRepository engagementRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private SlalomResourceRepository resourceRepository;

    @PreAuthorize("hasRole('ADMIN')")
    public boolean createEngagement( Engagement engagement) {
        Long clientId = engagement.getClient().getClientId();
        Client client = clientRepository.findOne(clientId);
        Long resourceId = engagement.getResource().getResourceId();
        SlalomResource resource = resourceRepository.findOne(resourceId);
        Engagement bench = findResourceBench(resource);
        int benchAllocation = bench.getAllocation();

        if(client !=null && resource!=null && engagement.getAllocation()>=0 && ( engagement.getAllocation()<=benchAllocation ) ){ //Check total allocation time
            engagement.setClient(client);
            engagement.setResource(resource);
            engagementRepository.save(engagement);
            bench.setAllocation(benchAllocation - engagement.getAllocation());
            engagementRepository.save(bench);
            return true;
        }
        return false; //Allocation is exceeding what bench current has, can not create new engagement
    }

    @PreAuthorize("hasRole('ADMIN')")
    public boolean updateEngagement(Engagement engagement) {

        Long clientId = engagement.getClient().getClientId();
        Client client = clientRepository.findOne(clientId);
        Long resourceId = engagement.getResource().getResourceId();
        SlalomResource resource = resourceRepository.findOne(resourceId);
        engagement.setClient(client);
        engagement.setResource(resource);
        Engagement previous = engagementRepository.findOne(engagement.getEngagementId());
        //Ensure the resource and client stays the same
        if(previous.getResource().equals(engagement.getResource()) && previous.getClient().equals(engagement.getClient())) {
            Engagement bench = findResourceBench(engagement.getResource());
            int benchAllocation = bench.getAllocation();
            int updatedAllocation = engagement.getAllocation();
            int previousAllocation = previous.getAllocation();
            int diff = previousAllocation - updatedAllocation;
            //The allocation change range is form 0 to what extra bench time can offer
            if ( updatedAllocation >= 0  &&  updatedAllocation<=(previousAllocation + benchAllocation) ) {
                engagement.setCreated(previous.getCreated());
                engagement.setCreatedBy(previous.getCreatedBy());
                engagement.setActive(previous.isActive());
                engagementRepository.save(engagement);
                bench.setAllocation(benchAllocation + diff);
                engagementRepository.save(bench);
                return true;
            }
            return false;
        }
        return false;

    }

    @PreAuthorize("hasRole('ADMIN')")
    public void deleteEngagement(Engagement engagement) {

        if(isBench(engagement.getEngagementId())){
            //This is a bench project
            //Every resource has an associated bench engagement by default. It get created when the resource joined Slalom. It get de-actived when the resource left.
            engagement.setActive(false);
            engagementRepository.save(engagement);
        }
        else {
            //This is not a bench project
            Engagement bench = findResourceBench(engagement.getResource());
            int benchAllocation = bench.getAllocation();

            bench.setAllocation(benchAllocation + engagement.getAllocation());
            engagementRepository.save(bench);

            //None bench engagement, de-active
            engagement.setAllocation(0);
            engagement.setEndDate(new Date());
            engagementRepository.save(engagement);
            engagementRepository.delete(engagement);
        }
    }

    public Iterable<Engagement> findAllEngagement() {

        return engagementRepository.findAll();
    }

    public Engagement findOneEngagement(Long engagementId) {

        return engagementRepository.findOne(engagementId);
    }

    public boolean isEngagementExist(Long engagementId) {

        return engagementRepository.exists(engagementId);
    }

    public boolean isBench(Long engagementId) {
        Engagement engagement = findOneEngagement(engagementId);
        if(engagement != null) {
            Client client = engagement.getClient();
            return bench.equalsIgnoreCase(client.getClientName());
        }
        else
            return false;
    }

    public Iterable<Engagement> findAllByResource(SlalomResource resource) {
        return engagementRepository.findAllEngagementsByResource(resource);
    }


    public Engagement findResourceBench(SlalomResource resource) {
        Iterable<Engagement> engagements = findAllByResource(resource);

        for(Engagement engagement : engagements) {
            if(bench.equalsIgnoreCase(engagement.getClient().getClientName())){
                return engagement;
            }
        }
        return null;
    }

}
