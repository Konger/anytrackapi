package com.anytrack.api.service;

import com.anytrack.api.domain.SlalomResource;


/**
 * Created by michael.kong on 2/8/16.
 */
public interface SlalomResourceService {


    boolean createResource( SlalomResource resource);


    boolean updateResource(SlalomResource resource);


    void deleteResource(SlalomResource resource);

    Iterable<SlalomResource>  findAllResources();

    SlalomResource findOneResource(Long resourceId);

    boolean isResourceExist(Long resourceId);

    String uploadImageToS3( String base64ImageString, String resourceName);

}
