package com.anytrack.api.service;

import com.anytrack.api.domain.Engagement;
import com.anytrack.api.domain.SlalomResource;

/**
 * Created by michael.kong on 2/5/16.
 */
public interface EngagementService {


     boolean createEngagement( Engagement engagement);


     boolean updateEngagement(Engagement engagement);


     void deleteEngagement(Engagement engagement);

     Iterable<Engagement>  findAllEngagement();

     Engagement findOneEngagement(Long engagementId);

     boolean isEngagementExist(Long engagementId);

     boolean isBench(Long engagementId);

     Iterable<Engagement> findAllByResource(SlalomResource resource);

     Engagement findResourceBench(SlalomResource resource);
}
