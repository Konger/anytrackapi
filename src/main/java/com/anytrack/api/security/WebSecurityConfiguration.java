package com.anytrack.api.security;

import com.anytrack.api.domain.UserAccount;
import com.anytrack.api.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by michael.kong on 2/1/16.
 */
@Configuration
 class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    UserAccountRepository userRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }

    @Bean
    UserDetailsService userDetailsService() {
        return new UserDetailsService() {

            @Override
            public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
                UserAccount user = userRepository.findByLogin(userName);

                if(user != null) {
                    //The User object is Spring Security UserDetails
                    return new User(user.getName(), user.getPassword(), true, true, true, true,
                            AuthorityUtils.createAuthorityList(user.getRole().getAuthority()));
                } else {
                    throw new UsernameNotFoundException(String.format("User with name=%s was not found", userName));
                }
            }
        };
    }
}