package com.anytrack.api.security;

import com.anytrack.api.domain.UserAccount;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by michael.kong on 2/9/16.
 */
public class SpringSecurityAuditorAware implements AuditorAware<UserAccount> {
    @Override
    public UserAccount getCurrentAuditor() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }

        return ((UserAccount) authentication.getPrincipal());
    }
}