package com.anytrack.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@Configuration
@ComponentScan("com.anytrack.api")
@EnableJpaRepositories  //Activate Spring Data JPA
@EnableTransactionManagement
@EnableAutoConfiguration( )

public class AnyTrackApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnyTrackApiApplication.class, args);
	}


	//Enable Global CORS support for the application
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurerAdapter() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("/**")
//						.allowedOrigins("http://localhost:8080")
//						.allowedMethods("PUT", "DELETE")
//						.allowedHeaders("header1", "header2")
//						.exposedHeaders("header1", "header2")
//						.allowCredentials(false).maxAge(3600);
//			}
//		};
//	}
}





