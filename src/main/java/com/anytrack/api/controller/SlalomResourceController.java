package com.anytrack.api.controller;

import com.anytrack.api.domain.SlalomResource;
import com.anytrack.api.service.SlalomResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by michael.kong on 2/8/16.
 */
@RestController
@RequestMapping("/slalomresources")
@Configuration
public class SlalomResourceController {
    @Autowired
    private SlalomResourceService resourceService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<SlalomResource> createSlalomResource(@RequestBody SlalomResource resource) {

        boolean result = resourceService.createResource(resource);
        if(result)
            return new ResponseEntity<SlalomResource>(resource, HttpStatus.CREATED);
        else
            return new ResponseEntity<SlalomResource>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping( method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Iterable<SlalomResource> >fetchSlalomResources( ) {

        Iterable<SlalomResource> resources = resourceService.findAllResources();
        return new ResponseEntity<Iterable<SlalomResource>>(resources, HttpStatus.OK);
    }

    @RequestMapping(value = "{resourceId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<SlalomResource> fetchSlalomResource(@PathVariable Long resourceId) {

        SlalomResource resource = resourceService.findOneResource(resourceId);
        if( resource != null)
            return new ResponseEntity<SlalomResource>(resource, HttpStatus.OK);
        else
            return new ResponseEntity<SlalomResource>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "{resourceId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<SlalomResource> deleteSlalomResource(@PathVariable Long resourceId ) {

        SlalomResource toDelete = resourceService.findOneResource(resourceId);
        if(toDelete != null) {
            resourceService.deleteResource(toDelete);
            return new ResponseEntity<SlalomResource>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<SlalomResource>(HttpStatus.NOT_FOUND); //response code 204
    }

    @RequestMapping(value = "/{resourceId}", method = RequestMethod.PUT)
    public ResponseEntity<SlalomResource> updateSlalomResource(@RequestBody SlalomResource resource, @PathVariable Long resourceId) {
        resource.setResourceId(resourceId);
        boolean result = resourceService.updateResource(resource);
        if(result)
            return new ResponseEntity<SlalomResource>(resource, HttpStatus.OK);  //response code 200
        else
            return new ResponseEntity<SlalomResource>(HttpStatus.BAD_REQUEST);
    }

}
