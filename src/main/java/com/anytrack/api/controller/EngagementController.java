package com.anytrack.api.controller;

import com.anytrack.api.domain.Engagement;
import com.anytrack.api.service.EngagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by michael.kong on 2/5/16.
 */
@RestController
@RequestMapping("/engagements")
@Configuration
public class EngagementController {

    @Autowired
    private EngagementService engagementService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Engagement> createEngagement(@RequestBody Engagement engagement) {
        if( engagement.getClient()==null || engagement.getResource()==null )
            return new ResponseEntity<Engagement>(HttpStatus.BAD_REQUEST);
        else {
            boolean result = engagementService.createEngagement(engagement);
            if (result)
                return new ResponseEntity<Engagement>(engagement, HttpStatus.CREATED);
            else
                return new ResponseEntity<Engagement>(HttpStatus.BAD_REQUEST); //We had invalid engagement time allocation
        }
    }

    @RequestMapping( method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Iterable<Engagement> >fetchEngagements( ) {

        Iterable<Engagement> engagements = engagementService.findAllEngagement();
        return new ResponseEntity<Iterable<Engagement>>(engagements, HttpStatus.OK);
    }

    @RequestMapping(value = "{engagementId}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Engagement> fetchEngagement(@PathVariable Long engagementId) {

        Engagement engagement = engagementService.findOneEngagement(engagementId);
        if( engagement != null)
            return new ResponseEntity<Engagement>(engagement, HttpStatus.OK);
        else
            return new ResponseEntity<Engagement>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "{engagementId}", method = RequestMethod.DELETE)
    public @ResponseBody
    ResponseEntity<Engagement> deleteEngagement(@PathVariable Long engagementId ) {

        Engagement toDelete = engagementService.findOneEngagement(engagementId);
        if(toDelete != null) {
            engagementService.deleteEngagement(toDelete);
            return new ResponseEntity<Engagement>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<Engagement>(HttpStatus.NOT_FOUND); //response code 204
    }

    @RequestMapping(value = "/{engagementId}", method = RequestMethod.PUT)
    public ResponseEntity<Engagement> updateEngagement(@RequestBody Engagement engagement, @PathVariable Long engagementId) {
        engagement.setEngagementId(engagementId);
        boolean result = engagementService.updateEngagement(engagement);
        if(result)
            return new ResponseEntity<Engagement>(engagement, HttpStatus.OK);  //response code 200
        else
            return new ResponseEntity<Engagement>(HttpStatus.BAD_REQUEST);
    }

}
