package com.anytrack.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Created by michael.kong on 2/1/16.
 */
@JsonIgnoreProperties({"active", "password"})
@Entity
@Table(name="user")
@Where(clause="active=1")
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", unique = true)
    private Long userId;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String name;

    @Column(nullable = true)
    private String token;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(nullable = false)
    private boolean active = true;

    //Constructors
    protected UserAccount(){} //default constructor for JPA

    public UserAccount( String login, String name, String password,  Role role ) {
        this.login = login;
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public UserAccount( UserAccount userAccount) {
        this.userId = userAccount.getUserId();
        this.login = userAccount.getLogin();
        this.name = userAccount.getName();
        this.password = userAccount.getPassword();
        this.role = userAccount.getRole();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId =  userId;
    }

    public String getName() {return name; }

    public void setName(String name) { this.name = name; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


}
