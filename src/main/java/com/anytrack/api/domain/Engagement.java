package com.anytrack.api.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael.kong on 2/1/16.
 */
@JsonIgnoreProperties({ "active", "created", "createdBy", "lastModifiedDate", "lastModifiedBy" })
@Entity
@Table(name="engagement")
@Where(clause="active=1")
public class Engagement extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "engagement_id", unique = true, nullable = false)
    private Long engagementId;

    @Column(nullable = false)
    private String projectName;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, optional = false)  //indicate the reference can not be null
    @JoinColumn(name = "client", nullable = false)
    private Client client;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)  //indicate the reference can not be null
    @JoinColumn(name = "resource", nullable = false)
    private SlalomResource resource;

    private Date startDate;

    private Date endDate;

    @Column(nullable = false)
    private int allocation=100;  //The time percentage of a resource spent on this engagement, can not exceed 100;

    //Constructors
    public Engagement( ){} //default constructor for JPA

    public Engagement(String projectName, Client client, SlalomResource resource, Date startDate, Date endDate, int allocation) {
        this.projectName = projectName;
        this.client = client;
        this.resource = resource;
        this.startDate = startDate;
        this.endDate = endDate;
        this.allocation = allocation;
    }

    public Long getEngagementId() {
        return engagementId;
    }

    public void setEngagementId(Long engagementId) {
        this.engagementId = engagementId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public SlalomResource getResource() {
        return resource;
    }

    public void setResource(SlalomResource resource) {
        this.resource = resource;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getAllocation() {
        return allocation;
    }

    public void setAllocation(int allocation) {
        this.allocation = allocation;
    }

    @Override
    public String toString() {
        return String.format(
                "Engagement[engagementId=%d, projectName='%s', client='%s', resource='%s',  allocation='%d']",
                engagementId, projectName, client.getClientName(), resource.getResourceName(), allocation);
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof Engagement) {
            Engagement that = (Engagement) other;
            result = (this.getEngagementId().equals(that.getEngagementId()) && this.getProjectName().equals(that.getProjectName()) && this.getClient().equals(that.getClient()) && this.getResource().equals(that.getResource()) && this.getAllocation()== that.getAllocation());
        }
        return result;
    }
}
