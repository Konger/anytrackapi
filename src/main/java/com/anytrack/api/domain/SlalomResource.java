package com.anytrack.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by michael.kong on 1/28/16.
 */
@JsonIgnoreProperties({ "active", "created", "createdBy", "lastModifiedDate", "lastModifiedBy" })
@Entity
@Table(name="resource")
@Where(clause="active=1")
public class SlalomResource extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resource_id", unique = true, nullable = false)
    private Long resourceId;

    @Value("${DEFAULT_RESOURCE_AVATAR}")
    @Column(nullable = false)
    private String avatarURL;

    @Column(nullable = false)
    private String resourceName;

    @Column(unique = true)
    private String resourceEmail;

    @Enumerated(EnumType.STRING)
    private Title title;

    @Column(nullable = true)
    private Date startDate;

    private boolean leadership = false;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
    private List<Engagement> projects = new ArrayList<Engagement>();


    //Constructors
    public SlalomResource( ){} //default constructor for JPA

    public SlalomResource(String resourceName, Title title, Date startDate, String resourceEmail, String avatarURL) {
        this.resourceName = resourceName;
        this.startDate = startDate;
        this.title = title;
        this.resourceEmail = resourceEmail;
        this.avatarURL = avatarURL;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceEmail() {
        return resourceEmail;
    }

    public void setResourceEmail(String resourceEmail) {
        this.resourceEmail = resourceEmail;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isLeadership() {
        return leadership;
    }

    public void setLeadership(boolean leadership) {
        this.leadership = leadership;
    }

    public List<Engagement> getProjects() {
        return projects;
    }

    public void setProjects(List<Engagement> projects) {
        this.projects = projects;
    }


    @Override
    public String toString() {
//        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//        String startDateString = df.format(startDate);
        return String.format(
                "SlalomResource[resourceId=%d, resourceName='%s', title='%s', resourceEmail='%s', avatarURL='%s']",
                resourceId, resourceName, title, resourceEmail, avatarURL);
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof SlalomResource) {
            SlalomResource that = (SlalomResource) other;
            result = (this.getResourceEmail().equalsIgnoreCase(that.getResourceEmail()) && this.getResourceName().equalsIgnoreCase(that.getResourceName()) && this.getTitle().equals(that.getTitle()) );
        }
        return result;
    }
}
