package com.anytrack.api.domain;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by michael.kong on 2/1/16.
 */
public enum Role implements GrantedAuthority {
    USER, ADMIN;

    @Override
    public String getAuthority() {
        return "ROLE_" + name();
    }
}
