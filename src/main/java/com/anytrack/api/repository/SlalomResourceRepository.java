package com.anytrack.api.repository;


import com.anytrack.api.domain.SlalomResource;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by michael.kong on 1/29/16.
 */
@RepositoryRestResource(collectionResourceRel = "slalomresources", path = "slalomresources")
public interface SlalomResourceRepository extends CrudRepository<SlalomResource, Long> {

    SlalomResource findByResourceEmail(@Param("resourceEmail") String resourceEmail);

    @Modifying
    @Query("update SlalomResource s set s.active=false where s.id=:#{#s.getResourceId()}")
    void delete(@Param("s") SlalomResource s);


}
