package com.anytrack.api.repository;

import com.anytrack.api.domain.Client;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.repository.query.Param;

/**
 * Created by michael.kong on 1/29/16.
 */
@RepositoryRestResource(collectionResourceRel = "clients", path = "clients")
public interface ClientRepository extends CrudRepository<Client, Long> {

    @Modifying
    @Query("update Client c set c.active=false where c.id=:#{#c.getClientId()}")
    void delete(@Param("c") Client c);

    Client findByClientName(@Param("clientName") String clientName);
//
//    @Modifying
//    @Query("update Client c set c.active=false where c.id=?1")
//    void delete(String id);

}
