package com.anytrack.api.repository;
import com.anytrack.api.domain.UserAccount;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by michael.kong on 2/1/16.
 */
@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {

    UserAccount findByLogin(@Param("login") String login);

    @Modifying
    @Query("update UserAccount u set u.active=false where u.id=:#{#u.getUserId()}")
    void delete(@Param("u") UserAccount u);

}
