package com.anytrack.api.repository;

import com.anytrack.api.domain.Engagement;
import com.anytrack.api.domain.SlalomResource;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by michael.kong on 2/2/16.
 */

//@RepositoryRestResource(exported = false)
@RepositoryRestResource(collectionResourceRel = "engagements", path = "engagements")
public interface EngagementRepository extends CrudRepository<Engagement, Long> {

    @Modifying
    @Query("update Engagement e set e.active=false where e.id=:#{#e.getEngagementId()}")
    void delete(@Param("e") Engagement e);

    @Query("select e from Engagement e where e.resource= ?1 and e.active=true")
    Iterable<Engagement> findAllEngagementsByResource(SlalomResource resource);




}
