/*create schema*/
create database IF NOT EXISTS any_track_db;
use any_track_db;

/*created two test users for authentication*/
insert into any_track_db.user ( login, name, password, role, active ) values ('testadmin@slalom.com', 'Test Admin', 'password', 'ADMIN', 1);
insert into any_track_db.user ( login, name, password, role, active ) values ('testuser@slalom.com', 'Test User', 'password', 'USER', 1);


/*populate the client table*/

insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('Free Agents (Bench)', now(), now(), 1); /*The name of bench is configured at the property file, the name should be exactly the same*/
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('HCSC', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('GGP', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('John Deere', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('Hyatt', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('Abbvie', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('TransUnion', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('JLL', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('ADM', now(), now(), 1);
insert into any_track_db.client ( client_name, created, last_modified_date, active ) values ('Coveris', now(), now(), 1);

/*populate the slalom resource table*/
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Tim McCarthy', 'PRACTICE_AREA_DIRECTOR', '2015-12-30', 'tim.mccarthy@slalom.com', 1, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Alban Mehmeti', 'PRACTICE_AREA_DIRECTOR', '2015-12-30', 'albanm@slalom.com', 1, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Boris Wexler', 'PRACTICE_AREA_LEAD', '2015-12-30', 'borisw@slalom.com', 1, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Damyant Gill', 'SOLUTION_PRINCIPAL', '2015-12-30', 'damyantg@slalom.com', 1, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Robert Wakerly', 'PRINCIPAL_CONSULTANT', '2015-12-30', 'robertw@slalom.com', 1, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Annabel Dunstone', 'DEVELOPER', '2015-12-30', 'annabel.dunstone@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Alicia Reyerson', 'UX_DESIGNER', '2015-12-30', 'aliciar@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Ben Edmiston', 'SOLUTION_ARCHITECT', '2015-12-30', 'ben.edmiston@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Charlotte Fernando', 'SOLUTION_OWNER', '2015-12-30', 'charlottef@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Dimitri Christodoulakis', 'SOLUTION_OWNER', '2015-12-30', 'dmitric@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Erika Hellauer', 'SOLUTION_OWNER', '2015-12-30', 'erikah@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Evan Scharfer', 'PRINCIPAL_CONSULTANT', '2015-12-30', 'evan.scharfer@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Ginett Colon', 'UX_DESIGNER', '2015-12-30', 'ginettc@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Jim Trahanas', 'SOLUTION_ANALYST', '2015-12-30', 'jimt@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Jeff Hassberger', 'SOLUTION_ANALYST', '2015-12-30', 'jeff.hassberger@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Jennifer Allen', 'SOLUTION_OWNER', '2015-12-30', 'jennifera@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Keith Quiring', 'SOLUTION_ANALYST', '2015-12-30', 'keithq@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Laura Nash', 'UX_DESIGNER', '2015-12-30', 'lauran@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Michael Kong', 'DEVELOPER', '2015-12-30', 'michael.kong@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Michael Simader', 'SOLUTION_OWNER', '2015-12-30', 'michael.simader@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Miheer Munjal', 'SOLUTION_ANALYST', '2015-12-30', 'miheerm@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Naveen Grover', 'PRINCIPAL_CONSULTANT', '2015-12-30', 'naveen.grover@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Nigel Caine', 'SOLUTION_OWNER', '2015-12-30', 'nigelc@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Roshan Soni', 'SOLUTION_ARCHITECT', '2015-12-30', 'roshans@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Ryan Gawel', 'SOLUTION_ANALYST', '2015-12-30', 'ryan.gawel@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Ryan Levy', 'SOLUTION_OWNER', '2015-12-30', 'ryan.levy@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Ryan Sullivan', 'SOLUTION_OWNER', '2015-12-30', 'ryan.sullivan@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Tyson Kunovsky', 'DEVELOPER', '2015-12-30', 'tyson.kunovsky@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Varsh Srivatsan', 'SOLUTION_ANALYST', '2015-12-30', 'varshs@slalom.com', 0, now(), now(), 1);
insert into any_track_db.resource ( resource_name, title, start_date, resource_email, leadership, created, last_modified_date, active ) values ('Will Smith', 'SOLUTION_OWNER', '2015-12-30', 'will.smith@slalom.com', 0, now(), now(), 1);

/*set up initial bench time*/
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 1, 100, now(), now(), 1);
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 2, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 3, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 4, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 5, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 6, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 7, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 8, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 9, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 10, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 11, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 12, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 13, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 14, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 15, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 16, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 17, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 18, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 19, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 20, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 21, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 22, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 23, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 24, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 25, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 26, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 27, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 28, 100, now(), now(), 1 );
insert into any_track_db.engagement ( project_name, client, resource, allocation, created, last_modified_date, active ) values ( 'Bench', 1, 29, 100, now(), now(), 1 );



