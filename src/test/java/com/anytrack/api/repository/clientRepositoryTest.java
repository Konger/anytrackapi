package com.anytrack.api.repository;

import com.anytrack.api.AbstractTest;
import com.anytrack.api.domain.Client;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by michael.kong on 2/9/16.
 */
@Transactional
public class clientRepositoryTest extends AbstractTest {

    @Value("${DEFAULT_CLIENT_AVATAR}")
    private String avatarURL;

    @Autowired
    private ClientRepository repository;


    @Before
    public void setUp() {
        //set up before test
    }

    @After
    public void tearDown() {
        // clean up after each test method
    }

    @Test
    public void testFindAll() {

        Iterable<Client> list = repository.findAll();

        Assert.assertNotNull("failure - expected not null", list);

    }

    @Test
    public void testFindOne() {

        Long id = new Long(1);

        Client entity = repository.findOne(id);

        Assert.assertNotNull("failure - expected not null", entity);
        Assert.assertEquals("failure - expected id attribute match", id,
                entity.getClientId());

    }

    @Test
    public void testFindOneNotFound() {

        Long id = Long.MAX_VALUE;

        Client entity = repository.findOne(id);

        Assert.assertNull("failure - expected null", entity);

    }

    @Test
    public void testCreate() {

        String clientName = "Test Client";
        Client entity = new Client( );
        entity.setClientName(clientName);
        entity.setAvatarURL(avatarURL);

        repository.save(entity);

        Assert.assertNotNull("failure - expected not null", entity);
        Assert.assertNotNull("failure - expected id attribute not null", entity.getClientId());
        Assert.assertEquals("failure - expected text attribute match", clientName, entity.getClientName());
        Assert.assertEquals("failure - expected int attribute match", avatarURL, entity.getAvatarURL());

    }


    @Test
    public void testUpdate() {

        Long id = new Long(2);

        Client entity = repository.findOne(id);

        Assert.assertNotNull("failure - expected not null", entity);

        String updatedClientName = entity.getClientName() + " test";
        entity.setClientName(updatedClientName);

        repository.save(entity);

        Assert.assertEquals("failure - expected id attribute match", id,
                entity.getClientId());
        Assert.assertEquals("failure - expected text attribute match",
                updatedClientName, entity.getClientName());

    }


    @Test
    public void testDelete() {
        //Soft delete not testable
    }
}
