package com.anytrack.api.service;

import com.anytrack.api.AbstractTest;
import com.anytrack.api.domain.Client;
import com.anytrack.api.domain.Engagement;
import com.anytrack.api.domain.SlalomResource;
import com.anytrack.api.repository.ClientRepository;
import com.anytrack.api.repository.SlalomResourceRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by michael.kong on 2/9/16.
 */

@Transactional
public class EngagementServiceTest extends AbstractTest {

    @Autowired
    private EngagementService service;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private SlalomResourceRepository resourceRepository;

    @Before
    public void setUp() {
        //set up before test
    }

    @After
    public void tearDown() {
        // clean up after each test method
    }

    @Test
    public void testFindAll() {

        Iterable<Engagement> list = service.findAllEngagement();

        Assert.assertNotNull("failure - expected not null", list);

    }

    @Test
    public void testFindOne() {

        Long id = new Long(1);

        Engagement entity = service.findOneEngagement(id);

        Assert.assertNotNull("failure - expected not null", entity);
        Assert.assertEquals("failure - expected id attribute match", id,
                entity.getEngagementId());

    }

    @Test
    public void testFindOneNotFound() {

        Long id = Long.MAX_VALUE;

        Engagement entity = service.findOneEngagement(id);

        Assert.assertNull("failure - expected null", entity);

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testCreate() {

        String projectName = "Test Engagement";
        Client client = clientRepository.findOne(new Long(2));
        SlalomResource resource = resourceRepository.findOne(new Long(2));
        Date startDate = new Date();
        Date endDate = new Date();
        int allocation = 66;
        Engagement entity = new Engagement(projectName, client, resource, startDate, endDate, allocation);

        boolean createdResult = service.createEngagement(entity);

        Assert.assertTrue("failure- expected true", createdResult);
        Assert.assertNotNull("failure - expected not null", entity);
        Assert.assertNotNull("failure - expected id attribute not null", entity.getEngagementId());
        Assert.assertEquals("failure - expected text attribute match", projectName, entity.getProjectName());
        Assert.assertEquals("failure - expected int attribute match", 66, entity.getAllocation());

        Iterable<Engagement> list = service.findAllEngagement();
        Assert.assertNotNull("failure - expected not null", list);

    }


    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testUpdate() {

        Long id = new Long(16);

        Engagement entity = service.findOneEngagement(id);

        Assert.assertNotNull("failure - expected not null", entity);

        String updatedProjectName = entity.getProjectName() + " test";
        entity.setProjectName(updatedProjectName);
        entity.setAllocation(10);
        boolean updatedResult = service.updateEngagement(entity);

        Assert.assertTrue("failure- expected true", updatedResult);
        Assert.assertEquals("failure - expected id attribute match", id,
                entity.getEngagementId());
        Assert.assertEquals("failure - expected text attribute match",
                updatedProjectName, entity.getProjectName());
        Assert.assertEquals("failure - expected int attribute match", 10, entity.getAllocation());

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testUpdateNotFound() {

        Exception exception = null;

        Engagement entity = new Engagement();
        entity.setEngagementId(Long.MAX_VALUE);
        entity.setProjectName("test");
        entity.setStartDate(new Date());
        Client client = clientRepository.findOne(new Long(2));
        entity.setClient(client);
        SlalomResource resource = resourceRepository.findOne(new Long(2));
        entity.setResource(resource);

        try {
            boolean updateResult = service.updateEngagement(entity);
        } catch (NullPointerException e) {
            exception = e;
        }

        Assert.assertNotNull("failure - expected exception", exception);
        Assert.assertTrue("failure - expected NullPointerException",
                exception instanceof NullPointerException);

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testDelete() {
        //Soft delete not testable
    }

}
