package com.anytrack.api.service;

/**
 * Created by michael.kong on 2/9/16.
 */

import com.anytrack.api.AbstractTest;
import com.anytrack.api.domain.SlalomResource;
import com.anytrack.api.domain.Title;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 * Unit test methods for the SlalomResourceService.
 *
 */
@Transactional
public class SlalomResourceServiceTest extends AbstractTest {

    @Autowired
    private SlalomResourceService service;

    @Before
    public void setUp() {
        //set up before test
    }

    @After
    public void tearDown() {
        // clean up after each test method
    }

    @Test
    public void testFindAll() {

        Iterable<SlalomResource> list = service.findAllResources();

        Assert.assertNotNull("failure - expected not null", list);


    }

    @Test
    public void testFindOne() {

        Long id = new Long(1);

        SlalomResource entity = service.findOneResource(id);

        Assert.assertNotNull("failure - expected not null", entity);
        Assert.assertEquals("failure - expected id attribute match", id,
                entity.getResourceId());

    }

    @Test
    public void testFindOneNotFound() {

        Long id = Long.MAX_VALUE;

        SlalomResource entity = service.findOneResource(id);

        Assert.assertNull("failure - expected null", entity);

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testCreate() {

        String resourceName = "John Doe";
        Title title = Title.DEVELOPER;
        Date startDate = new Date();
        String resourceEmail = "jdoe@slalom.com";
        String avatarURL = null;
        SlalomResource entity = new SlalomResource(resourceName, title, startDate, resourceEmail, avatarURL);

        boolean createdResult = service.createResource(entity);

        Assert.assertTrue("failure- expected true", createdResult);
        Assert.assertNotNull("failure - expected not null", entity);
        Assert.assertNotNull("failure - expected id attribute not null", entity.getResourceId());
        Assert.assertEquals("failure - expected text attribute match", "jdoe@slalom.com", entity.getResourceEmail());

        Iterable<SlalomResource> list = service.findAllResources();
        Assert.assertNotNull("failure - expected not null", list);
        //Assert.assertEquals("failure - expected size", 3, list.size());

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testCreateWithDuplicatedEmail() {

        SlalomResource entity = new SlalomResource();
        entity.setResourceId(Long.MAX_VALUE);
        entity.setResourceName("Michael Kong");
        entity.setResourceEmail("michael.kong@slalom.com");

        boolean createResult = service.createResource(entity);

        Assert.assertFalse("failure - expected false", createResult);

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testUpdate() {

        Long id = new Long(31);

        SlalomResource entity = service.findOneResource(id);

        Assert.assertNotNull("failure - expected not null", entity);

        String updatedName = entity.getResourceName() + " test";
        entity.setResourceName(updatedName);
        boolean updatedResult = service.updateResource(entity);

        Assert.assertTrue("failure- expected true", updatedResult);
        Assert.assertEquals("failure - expected id attribute match", id,
                entity.getResourceId());
        Assert.assertEquals("failure - expected text attribute match",
                updatedName, entity.getResourceName());

    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testUpdateNotFound() {

        SlalomResource entity = new SlalomResource();
        entity.setResourceId(Long.MAX_VALUE);
        entity.setResourceName("test");
        entity.setResourceEmail("testupdate@slalom.com");
        entity.setStartDate(new Date());

        boolean updateResult = service.updateResource(entity);

        Assert.assertFalse("failure - expected false", updateResult);
    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void testDelete() {
        //Not testable for soft delete, due to the inactive resources are filtered out
    }

}
