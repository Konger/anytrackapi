# README #


### What is this repository for? ###
* This is a Spring RESTful Web Service providing data for Any Track
* Version 1
*

### How do I get set up? ###

* Summary of set up
All endpoints has been locked down with Spring Security. The app authenticate by checking users against a user_account table in database.
It is important to have the database ready before you start to run the app locally.

1. Set up your database

2. Update your database connection info at localdev.properties

3. Create the any_track_db schema in your database by running the following query:
        create database IF NOT EXISTS any_track_db;
        use any_track_db;

4. Start the app by running this at the terminal/console:
        mvn spring-boot:run

5. Once the app is running, JPA/Hibernate will generate all database tables based on domain object's annotations. There is no need to create any tables yourself.

6. Once all the tables are created, populate the tables by running queries in sql_import.sql.
Make sure you have run the following queries to create two test users

        /*create two test users for authentication*/
       insert into any_track_db.user ( login, name, password, role, active ) values ('testadmin@slalom.com', 'Test Admin', 'password', 'ADMIN', 1);
       insert into any_track_db.user ( login, name, password, role, active ) values ('testuser@slalom.com', 'Test User', 'password', 'USER', 1);

7. You can now log into the app with either testuser or testadmin

        URL: http://www.localhost:8080/any-track-api/v1/profile/

Since HATEOAS is enabled, you can navigate the API from your browser by clicking various HATEOAS links.
For best result, you can use POSTMAN to make and receive HTTP calls.

* Configuration
The app is set to use the localdev profile, you can change the profile setting at application.properties


* Dependencies



* Database configuration
Changing the active profile to update the database setting


* How to run tests



* Deployment instructions




### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin:  zgkong@gmail.com